# Gitlab-ci et gitflow
Git est un outil de versionning très utilisé, et il est nécessaire d'en faire bonne usage afin de profiter au maximum du CI.
## Gitflow
https://git.ouest.innovation.insee.eu/innovation/exemples/gitflow
##  L'intégration continue
### Introduction 
A l'origine, il y avait l'idée que ça serait pas mal de pouvoir partir 10 minutes plus tôt tous les jours, en effectuant la compilation du projet a distance.
Du coup il y a eu l'idée d'automatiser tout cela, et par compilation on entend bien évidemment le fait que le code tourne, mais qu'il tourne aussi fonctionnellement et donc qu'il puisse être testé a distance.

### Pourquoi gitlab ci ?
- C'est très utilisé a l'insee.
- C'est assez pratique d'avoir dans l'utilitaire de gestion de version un outil intégré pour l'intégration (On a pas à ouvrir une fenêtre externe comme avec travis ou circle ci)


### Premieres expériences avec le CI sur gitlab
Une fois gitlab bien configuré, forkez le projet puis clonez le et déplacez vous sur la branche exo-1 
cc : 
```
git clone https://gitlab.com/[Votre pseudo]/api-integration-example.git 
cd api-integration-example
git checkout exo-1
```

### Présentation de l'application
- Petite application java de génération de personnages, appli très simple microservice avec CRUD gérés par spring data jpa.
- api disponible a cette adresse une fois le serveur lancé : [http://localhost:8080/swagger-ui/index.html?url=/v3/api-docs](http://localhost:8080/swagger-ui/index.html?url=/v3/api-docs)



## Rappels sur Maven
![Cycle de vie](img/maven.jpg)
## GitLab CI
### Introduction
Le fichier .gitlab-ci.yml présent à la racine du projet permet de programmer des jobs qu'on souhaite lancer à chaque push.
> yml <=> json
##### stages
Les "stages" sont les étapes, elles permettent d'ordonner les jobs, les jobs d'une même étape peuvent s'exécuter en parallèle, on a habituellement les étapes build -> test -> deploy
#####  variables
On définit les variables utilisées plus tard dans la section variables, notamment l'endroit où on dépose les images docker qu'on va construire, à l'heure actuelle, les images sont déposées sur docker-registry.beta.innovation.insee.eu
##### job
Pour chaque job, on lui définit un nom, puis on précise le stage, le tags (maven pour tomcat et java, shell pour postgres), les scripts pour build l'image docker ou pour la déployer.
Pour construire une image Tomcat, d'abord on doit créer un war à partir du code source avec la commande mvn package, ensuite on construit l'image docker avec la commande docker build lancée depuis le dossier où se trouve le fichier Dockerfile qui précise les spécifications de l'image, puis on envoie l'image construite sur docker-registry.
### Etablissement d'un pipeline pour l'application
(vous pouvez désormais travailler sur votre poste en réimportant le projet )
- Créez donc un fichier .gitlab-ci.yml a la racine.